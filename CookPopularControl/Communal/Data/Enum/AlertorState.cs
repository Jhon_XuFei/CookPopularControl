﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



/*
 * Copyright (c) 2021 All Rights Reserved.
 * Description：AlertorState
 * Author： Chance_写代码的厨子
 * Create Time：2021-06-10 10:43:29
 */
namespace CookPopularControl.Communal.Data.Enum
{
    /// <summary>
    /// 警报器状态
    /// </summary>
    public enum AlertorState
    {
        Normal,
        Success,
        Warning,
        Error,
        Fatal
    }
}
